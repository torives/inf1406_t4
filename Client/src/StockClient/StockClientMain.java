package StockClient;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;

import org.omg.CORBA.ORB;

import StockMarket.StockExchange;
import StockMarket.StockExchangeHelper;
import StockMarket.StockInfo;
import StockMarket.StockInfoHelper;
import StockMarket.StockServer;
import StockMarket.StockServerHelper;
import StockMarket.UnknownSymbol;

public class StockClientMain {

	private static String iorSourceFilePath;

	public static void main(String[] args) throws IOException {

		if(args.length != 1){
			System.out.println("Incorrect number of arguments. Execution canceled.");
			System.exit(1);
		}

		try{
			iorSourceFilePath = args[0];
			File iorIn = new File(iorSourceFilePath);
			iorIn = null;
		}catch (NullPointerException e){
			System.out.println("Could'n find file at the designated path. Execution canceled");
			System.exit(1);
		}

		// As propriedades que informam o uso do JacORB como ORB.
		Properties orbProps = new Properties();
		orbProps.setProperty("org.omg.CORBA.ORBClass", "org.jacorb.orb.ORB");
		orbProps.setProperty("org.omg.CORBA.ORBSingletonClass","org.jacorb.orb.ORBSingleton");

		// Inicializa o ORB
		ORB orb = ORB.init(args, orbProps);

		//Exporta a fábrica de StockInfo
		orb.register_value_factory(StockInfoHelper.id(), new StockInfoFactory());		

		// Lê o IOR do arquivo cujo nome é passado como parâmetro
		Scanner scanner = new Scanner(new File(iorSourceFilePath));
		String stockExchangeIOR = scanner.nextLine();
		String stockServerIOR = scanner.nextLine();
		scanner.close();

		// Obtém a referência para o StockServer
		org.omg.CORBA.Object stockObj = orb.string_to_object(stockServerIOR);
		StockServer stockServer = StockServerHelper.narrow(stockObj);

		org.omg.CORBA.Object exchangeObj = orb.string_to_object(stockExchangeIOR);
		StockExchange exchangeServer = StockExchangeHelper.narrow(exchangeObj);


		//Faz chamadas remotas
		try {
			retrieveStockInfo(stockServer);
			buyStock(exchangeServer, "AAA");
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		} catch (UnknownSymbol e) {
			System.out.println("Could not find stock with given name");
		}
	}

	/*
	 * 	Imprime todas as ações e seus valores
	 */
	private static void retrieveStockInfo(StockServer stockServer) throws InterruptedException{		
		StockInfo[] infoArray = null;

		try{
			infoArray = stockServer.getStocksInfo();

			for (StockInfo info : infoArray ){
				System.out.println(info._toString());
			}

		}
		catch(org.omg.CORBA.TRANSIENT e){

			for(int attempts=3; attempts>0; attempts--){

				Thread.sleep(1000);
				infoArray = stockServer.getStocksInfo();

				if(infoArray != null){

					for (StockInfo info : infoArray ){
						System.out.println(info._toString());
					}
					return;
				}
			}
			System.out.println("Connection problem: could not retrieve stock symbols.");
		}
		catch(org.omg.CORBA.COMM_FAILURE e){

			for(int attempts=3; attempts>0; attempts--){

				Thread.sleep(1000);
				infoArray = stockServer.getStocksInfo();

				if(infoArray != null){

					for (StockInfo info : infoArray ){
						System.out.println(info._toString());
					}
					return;
				}
			}
			System.out.println("Connection problem: could not retrieve stock symbols.");
		}
		catch(org.omg.CORBA.OBJECT_NOT_EXIST e){
			System.out.println("Internal server error: could not retrieve stock information.");
		}
	}

	/*
	 * Compra uma ação
	 */
	private static void buyStock(StockExchange exchangeServer, String stock) throws InterruptedException, UnknownSymbol{

		try {
			exchangeServer.buyStock("AAA");

		}catch(org.omg.CORBA.TRANSIENT e){

			for(int attempts=3; attempts>0; attempts--){

				Thread.sleep(1000);

				if(exchangeServer.buyStock("AAA")){
					return;
				}
			}
			System.out.println("Connection problem: could not retrieve stock symbols.");
		}
		catch(org.omg.CORBA.COMM_FAILURE e){
			
			for(int attempts=3; attempts>0; attempts--){

				Thread.sleep(1000);

				if(exchangeServer.buyStock("AAA")){
					return;
				}
			}
			System.out.println("Network problem: could not connect to the stock exchange server.");
		}
		catch(org.omg.CORBA.OBJECT_NOT_EXIST e){
			System.out.println("Internal server problem: could not finish transaction.");
		}       
	}

	/** 
	 *  Chama getStockSymbols
	 */

	//	String[] stockSymbols = null;
	//
	//	try{
	//		stockSymbols = stockServer.getStockSymbols();
	//		System.out.println(stockSymbols.toString());
	//	}
	//	catch(org.omg.CORBA.TRANSIENT e){
	//		
	//		for(int attempts=3; attempts>0; attempts--){
	//			
	//			Thread.sleep(1000);
	//			stockSymbols = stockServer.getStockSymbols();
	//			
	//			if(stockSymbols != null){
	//				System.out.println(stockSymbols.toString());
	//				break;
	//			}
	//		}
	//		System.out.println("Connection problem: could not retrieve stock symbols.");
	//	}
	//	catch(org.omg.CORBA.COMM_FAILURE e){
	//		
	//		for(int attempts=3; attempts>0; attempts--){
	//			
	//			Thread.sleep(1000);
	//			stockSymbols = stockServer.getStockSymbols();
	//			
	//			if(stockSymbols != null){
	//				System.out.println(stockSymbols.toString());
	//				break;
	//			}
	//		}
	//		System.out.println("Connection problem: could not retrieve stock symbols.");
	//	}
	//	catch(org.omg.CORBA.OBJECT_NOT_EXIST e){
	//		System.out.println("Internal server problem: could not retrieve stock symbols.");
	//	}
}