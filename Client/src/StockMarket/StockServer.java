package StockMarket;

/**
 * Generated from IDL interface "StockServer".
 *
 * @author JacORB IDL compiler V 3.1, 19-Aug-2012
 * @version generated at 25/11/2015 14:17:15
 */

public interface StockServer
	extends StockServerOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
