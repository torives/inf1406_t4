package StockLogger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.Scanner;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import StockMarket.ExchangePrinter;
import StockMarket.ExchangePrinterHelper;
import StockMarket.ExchangePrinterPOATie;
import StockMarket.StockExchange;
import StockMarket.StockExchangeHelper;

public class StockLoggerMain {

	private static String iorSourceFilePath;
	private static File stockRecord;
	private static DisplayExchangePrinter displayPrinter;
	private static FileExchangePrinter filePrinter;
	
	public static void main(String[] args) {

		if(args.length != 2){
			System.out.println("Incorrect number of arguments. Execution canceled.");
			System.exit(1);
		}
	
		try{
			iorSourceFilePath = args[0];
			File testFile = new File(iorSourceFilePath);
			testFile = null;
			stockRecord = new File(args[1]);
		}catch (NullPointerException e){
			System.out.println("Could'n find file at the designated path. Execution canceled");
			System.exit(1);
		}
		
		// As propriedades que informam o uso do JacORB como ORB.
		Properties orbProps = new Properties();
		orbProps.setProperty("org.omg.CORBA.ORBClass", "org.jacorb.orb.ORB");
		orbProps.setProperty("org.omg.CORBA.ORBSingletonClass","org.jacorb.orb.ORBSingleton");

		// Inicializa o ORB.
		ORB orb = ORB.init(args, orbProps);
			
		//Instancia os servants
		displayPrinter = new DisplayExchangePrinter();
		filePrinter = new FileExchangePrinter(stockRecord);
		ExchangePrinterPOATie displayPrinterPOA = new ExchangePrinterPOATie(displayPrinter);
		ExchangePrinterPOATie filePrinterPOA = new ExchangePrinterPOATie(filePrinter);
		
		//Exporta os servants
		POA poa;
		try {
			poa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			poa.the_POAManager().activate();
			
			org.omg.CORBA.Object displayPrinterObject = poa.servant_to_reference(displayPrinterPOA);
			org.omg.CORBA.Object filePrinterObject = poa.servant_to_reference(filePrinterPOA);
			
			ExchangePrinter displayPrinterReference = ExchangePrinterHelper.narrow(displayPrinterObject);
			ExchangePrinter filePrinterReference = ExchangePrinterHelper.narrow(filePrinterObject);
		
			
			// Lê o IOR do arquivo cujo nome é passado como parâmetro
			String ior = null;
			
			try {
				Scanner scanner = new Scanner(new File(iorSourceFilePath));
				//Assume que StockExchange é o primeiro IOR listado no arquivo
				ior = scanner.nextLine();
				scanner.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			// Obtém a referência para o ExchangeServer
			org.omg.CORBA.Object obj = orb.string_to_object(ior);
			StockExchange exchangeServer = StockExchangeHelper.narrow(obj);			
			
			//Adiciona as impressoras ao StockExchange
			try{
				connectPrinter(filePrinterReference, exchangeServer);
				connectPrinter(displayPrinterReference, exchangeServer);
			}
			catch(InterruptedException e){
				e.printStackTrace();
			}
		} 
		catch (InvalidName e) {
		
			e.printStackTrace();
		}
		catch (AdapterInactive e) {
		
			e.printStackTrace();
		}
		catch (ServantNotActive e) {
			
			e.printStackTrace();
		} 
		catch (WrongPolicy e) {
			e.printStackTrace();
		}
		
		orb.run();
		
	}
	
	private static void connectPrinter(ExchangePrinter printer, StockExchange exchangeServer) throws InterruptedException{
		
		try{
			exchangeServer.connectPrinter(printer);
		}
		catch(org.omg.CORBA.TRANSIENT e){
				
			for(int attempts=3; attempts>0; attempts--){
			
				Thread.sleep(1000);
	
				if(exchangeServer.connectPrinter(printer)){
					return;
				}
			}
			System.out.println("Network problem: could not connect printer.");
		}
		catch(org.omg.CORBA.COMM_FAILURE e){
			
			for(int attempts=3; attempts>0; attempts--){
				
				Thread.sleep(1000);

				if(exchangeServer.connectPrinter(printer)){
					return;
				}
			}
			System.out.println("Network problem: could not connect printer.");
		}
		catch(org.omg.CORBA.OBJECT_NOT_EXIST e){
			System.out.println("Could not connect printer. Server was not found");
		}
	}
}
