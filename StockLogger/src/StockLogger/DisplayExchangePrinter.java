package StockLogger;

import StockMarket.ExchangePrinterOperations;

public class DisplayExchangePrinter implements ExchangePrinterOperations {

	@Override
	public void print(String symbol) {
		System.out.println("Stock "+symbol+" has been bought");
	}

}