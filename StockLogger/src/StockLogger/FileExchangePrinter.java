package StockLogger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import StockMarket.ExchangePrinterOperations;

public class FileExchangePrinter implements ExchangePrinterOperations {

	private File out;
	
	public FileExchangePrinter(File out){
		this.out = out;
	}
	
	@Override
	public void print(String symbol) {
		
		try {
			FileWriter writer = new FileWriter(out, true);
			writer.write("Stock "+symbol+" has been bought");
			writer.close();
			System.out.println("Transaction saved to file");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
