package StockMarket;

/**
 * Generated from IDL exception "UnknownSymbol".
 *
 * @author JacORB IDL compiler V 3.1, 19-Aug-2012
 * @version generated at 25/11/2015 17:51:56
 */

public final class UnknownSymbol
	extends org.omg.CORBA.UserException
{
	/** Serial version UID. */
	private static final long serialVersionUID = 1L;
	public UnknownSymbol()
	{
		super(StockMarket.UnknownSymbolHelper.id());
	}

	public UnknownSymbol(String value)
	{
		super(value);
	}
}
