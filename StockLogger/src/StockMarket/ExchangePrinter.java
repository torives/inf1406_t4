package StockMarket;

/**
 * Generated from IDL interface "ExchangePrinter".
 *
 * @author JacORB IDL compiler V 3.1, 19-Aug-2012
 * @version generated at 25/11/2015 17:51:56
 */

public interface ExchangePrinter
	extends ExchangePrinterOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
