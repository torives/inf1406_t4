package StockServer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import StockMarket.StockExchangePOATie;
import StockMarket.StockServerPOATie;


public class StockSellerMain {

	private static ORB orb;
	private static StockSellerImpl servant;
	
	//TODO treat exceptions correctly
	public static void main(String[] args) {
		
		if(args.length != 1){
			System.out.println("Incorrect number of arguments. Execution canceled.");
			System.exit(1);
		}
		
		String iorRecordPath = args[0];
	
		try{
			File testFile = new File(iorRecordPath);
			testFile = null;
		}catch (NullPointerException e){
			System.out.println("Could'n find file at the designated path. Execution canceled");
			System.exit(1);
		}
		
		
		//Inicializa o ORB
		Properties orbProps = new Properties();
		orbProps.setProperty("org.omg.CORBA.ORBClass", "org.jacorb.orb.ORB");
		orbProps.setProperty("org.omg.CORBA.ORBSingletonClass", "org.jacorb.orb.ORBSingleton");
		orb = ORB.init(args, orbProps);
		
		//Instancia o servant
		servant = new StockSellerImpl();
		StockServerPOATie stockServer = new StockServerPOATie(servant);
		StockExchangePOATie stockExchange = new StockExchangePOATie(servant);
		
		//Exporta o servant
		POA poa;
		try {
			poa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			poa.the_POAManager().activate();
			org.omg.CORBA.Object serverObject = poa.servant_to_reference(stockServer);
			org.omg.CORBA.Object exchangeObject = poa.servant_to_reference(stockExchange);
			
			ArrayList<org.omg.CORBA.Object> objects = new ArrayList<org.omg.CORBA.Object>();
			objects.add(exchangeObject);
			objects.add(serverObject);
			//Salva IOR do servant em um arquivo visivel para o cliente
			writeRemoteObjectsIORToFileAtPath(iorRecordPath, objects);
			
			orb.run();
		} 
		catch (InvalidName e) {
		
			e.printStackTrace();
		}
		catch (AdapterInactive e) {
		
			e.printStackTrace();
		}
		catch (ServantNotActive e) {
			
			e.printStackTrace();
		} 
		catch (WrongPolicy e) {
			e.printStackTrace();
		}
	}
	
	private static void writeRemoteObjectsIORToFileAtPath(String path, ArrayList<org.omg.CORBA.Object> remoteObjects){
		
		try {
			FileWriter writer = new FileWriter(path);
			
			for(org.omg.CORBA.Object obj: remoteObjects ){
				writer.write(orb.object_to_string(obj)+"\n");
			}
			
			writer.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
