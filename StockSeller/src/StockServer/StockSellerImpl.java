package StockServer;

import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Semaphore;

import StockMarket.ExchangePrinter;
import StockMarket.StockExchangeOperations;
import StockMarket.StockInfo;
import StockMarket.StockInfoImpl;
import StockMarket.StockServerOperations;
import StockMarket.UnknownSymbol;

//TODO tratar concorrencia
public class StockSellerImpl implements StockServerOperations, StockExchangeOperations {

	
    private ConcurrentMap<String, Float> myStock;
    private Semaphore mutex;
    private ConcurrentLinkedQueue<ExchangePrinter> printers = new ConcurrentLinkedQueue<ExchangePrinter>();
 
    public StockSellerImpl() {

    	mutex = new Semaphore(1);
    	myStock = new ConcurrentHashMap<String,Float>();
        // Inicializa as ações com nomes e valores
        // fixos ou atribuídos randomicamente
    	
    	myStock.putIfAbsent("AAA", 34.4f);
    	myStock.putIfAbsent("BBB", 21.5f);
    	myStock.putIfAbsent("CCC", 14.0f);
	}
    
    
    /**
	 * StockServerOperations Methods
	 */
	
	public float getStockValue(String symbol) {
		
		if (myStock.containsKey(symbol)) {
			try {
	
				//como o valor da ação pode mudar, temos que garantir
				//que ninguém está alterando esse valor agora
				
				mutex.acquire();
			} catch (InterruptedException e) {
				//TODO
				e.printStackTrace();
			}
			
			float value = myStock.get(symbol);
			mutex.release();
            
			return value;
        }
		else {
        	return 0f; 
        }
	}

	public String[] getStockSymbols() {
		
		//como nenhuma stock sera registrada no ciclo de vida do programa,
		//o keySet nao vai mudar e ta td ok
		
		Set<String> keys = myStock.keySet();
		return keys.toArray(new String[0]);
	}

	public StockInfo[] getStocksInfo() {
		
		int index = 0;
		StockInfo[] infoArray = new StockInfo[myStock.size()];
		
		try {
			mutex.acquire();
			
			for (Entry<String, Float> entry : myStock.entrySet()){

				String key = entry.getKey();
				Float value = entry.getValue();
				infoArray[index++] = new StockInfoImpl(key, value);
			}	
			mutex.release();
		} 
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return infoArray;
	}

	
	/**
	 * StockExchangeOperations Methods
	 */
	
	public boolean buyStock(String symbol) throws UnknownSymbol {
		
		if (myStock.containsKey(symbol)) {
   
			try {
				mutex.acquire();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			float oldValue = myStock.get(symbol);
			myStock.replace(symbol, oldValue*1.1f);
			
			for(ExchangePrinter printer: printers){

				try{
					printer.print(symbol);
				}
				catch(org.omg.CORBA.TRANSIENT e){
					
					System.out.println("Message to printer " +printer.toString()+" failed");
					printer.print(symbol);
				}
				catch(org.omg.CORBA.COMM_FAILURE e){
					
					System.out.println("Message to printer " +printer.toString()+" failed");
					printer.print(symbol);
				}
				catch(org.omg.CORBA.OBJECT_NOT_EXIST e){
							
					printers.remove(printer);
					System.out.println("Could not find printer " +printer.toString()+". Printer removed");
				}
			}
			mutex.release();
			return true;
        }
		else {
        	throw new UnknownSymbol(); 
        }
	}

	public boolean connectPrinter(ExchangePrinter printer) {
		
		//if(!this.printers.contains(printer)){

			this.printers.add(printer);
			System.out.println("Printer "+printer.toString()+" was connected.");
		//}
		
		return true;
	}
}
