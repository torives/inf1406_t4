package StockMarket;

public class StockInfoImpl extends StockInfo {

	public StockInfoImpl(){
		
	}
	
	public StockInfoImpl(String name, float value){
		this.name = name;
		this.value = value;
	}
	
	@Override
	public String _toString() {
		
		return "Stock Information - name: " + this.name + " value: " + this.value;
	}
}
