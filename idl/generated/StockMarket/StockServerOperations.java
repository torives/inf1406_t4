package StockMarket;


/**
 * Generated from IDL interface "StockServer".
 *
 * @author JacORB IDL compiler V 3.1, 19-Aug-2012
 * @version generated at 25/11/2015 17:51:56
 */

public interface StockServerOperations
{
	/* constants */
	/* operations  */
	float getStockValue(java.lang.String symbol);
	java.lang.String[] getStockSymbols();
	StockMarket.StockInfo[] getStocksInfo();
}
