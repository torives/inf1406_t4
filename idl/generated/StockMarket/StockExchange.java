package StockMarket;

/**
 * Generated from IDL interface "StockExchange".
 *
 * @author JacORB IDL compiler V 3.1, 19-Aug-2012
 * @version generated at 25/11/2015 17:51:56
 */

public interface StockExchange
	extends StockExchangeOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
